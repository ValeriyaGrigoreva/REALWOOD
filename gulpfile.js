var gulp = require("gulp");
var sass = require("gulp-sass");

gulp.task("css", function () {
  gulp.src("scss/style.scss")
    .pipe(sass()) // Using gulp-sass
    .pipe(gulp.dest('dist'))


});
gulp.task("watch", function () {
  gulp.watch("scss/*", ["css"]);

});


gulp.task("default", [ "css" ]);